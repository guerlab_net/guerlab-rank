package net.guerlab.rank;

/**
 * 排名类
 *
 * @author guer
 *
 * @param <T>
 *            排名类类型
 * @param <V>
 *            权重类型
 */
public interface RankObject<T extends RankObject<T, V>, V extends Comparable<V>> extends Comparable<T> {

    /**
     * 返回权重
     *
     * @return 权重
     */
    V weight();

    @Override
    default int compareTo(
            T o) {
        return 0 - weight().compareTo(o.weight());
    }
}
