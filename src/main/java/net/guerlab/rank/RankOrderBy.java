package net.guerlab.rank;

import java.util.Comparator;

/**
 * 排序方式
 *
 * @author guer
 *
 */
public enum RankOrderBy {

    /**
     * 正序
     */
    ASC {

        @Override
        public <T extends Comparable<T>> Comparator<T> comparator() {
            return (
                    o1,
                    o2) -> 0 - o1.compareTo(o2);
        }
    },

    /**
     * 倒序
     */
    DESC {

        @Override
        public <T extends Comparable<T>> Comparator<T> comparator() {
            return (
                    o1,
                    o2) -> o1.compareTo(o2);
        }
    };

    /**
     * 获取排序方法
     *
     * @param <T>
     *            排序类
     *
     * @return 排序方法
     */
    public abstract <T extends Comparable<T>> Comparator<T> comparator();
}
