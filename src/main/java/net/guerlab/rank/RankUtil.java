package net.guerlab.rank;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 排名工具类
 *
 * @author guer
 *
 */
public class RankUtil {

    private RankUtil() {

    }

    /**
     * 正序排名
     *
     * @param list
     *            需要排名的集合
     * @param function
     *            设置排名值的方法
     * @param <R>
     *            需要排名的元素类型
     * @param <V>
     *            元素的权重类型
     * @return 排名后的集合
     */
    public static <R extends RankObject<R, V>, V extends Comparable<V>> List<R> rankAsc(
            List<R> list,
            SetRankFunction<R> function) {
        return rank(list, function, RankOrderBy.ASC.comparator());
    }

    /**
     * 倒序排名
     *
     * @param list
     *            需要排名的集合
     * @param function
     *            设置排名值的方法
     * @param <R>
     *            需要排名的元素类型
     * @param <V>
     *            元素的权重类型
     * @return 排名后的集合
     */
    public static <R extends RankObject<R, V>, V extends Comparable<V>> List<R> rankDesc(
            List<R> list,
            SetRankFunction<R> function) {
        return rank(list, function, RankOrderBy.DESC.comparator());
    }

    /**
     * 自定义排名
     *
     * @param list
     *            需要排名的集合
     * @param function
     *            设置排名值的方法
     * @param comparator
     *            排名大小计算规则
     * @param <R>
     *            需要排名的元素类型
     * @param <V>
     *            元素的权重类型
     * @return 排名后的集合
     */
    public static <R extends RankObject<R, V>, V extends Comparable<V>> List<R> rank(
            List<R> list,
            SetRankFunction<R> function,
            Comparator<R> comparator) {
        if (list == null || list.isEmpty()) {
            return Collections.emptyList();
        }

        if (comparator == null) {
            throw new NullPointerException("comparator cann't be null");
        }

        long rank = 1;
        long equals = 0;
        V nowWeight = null;

        List<R> sortedList = list.stream().sorted(comparator).collect(Collectors.toList());

        for (R obj : sortedList) {
            V weight = obj.weight();

            if (nowWeight != null && nowWeight.compareTo(weight) == 0) {
                equals++;
            } else {
                rank += equals;
                equals = 1;
            }

            function.set(obj, rank);
            nowWeight = weight;
        }

        return sortedList;
    }
}
