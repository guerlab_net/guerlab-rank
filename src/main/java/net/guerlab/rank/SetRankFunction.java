package net.guerlab.rank;

/**
 * 设置排名函数
 *
 * @author guer
 *
 * @param <R>
 *            排名类类型
 */
@FunctionalInterface
public interface SetRankFunction<R extends RankObject<R, ?>> {

    /**
     * 设置排名
     * 
     * @param obj
     *            排名类
     * @param rank
     *            排名值
     */
    void set(
            R obj,
            long rank);
}
