package net.guerlab.rank;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class RankTest {

    public static void main(
            String[] args) {

        List<DemoRankObject> list = new ArrayList<>(10);

        for (int i = 0; i < 10; i++) {
            list.add(new DemoRankObject(new Random().nextInt()));
        }

        System.out.println("desc");

        RankUtil.rankDesc(list, (
                obj,
                rank) -> obj.setRank(rank)).stream().forEach(System.out::println);

        System.out.println("asc");

        RankUtil.rankAsc(list, (
                obj,
                rank) -> obj.setRank(rank)).stream().forEach(System.out::println);
    }

    public static class DemoRankObject implements RankObject<DemoRankObject, Integer> {

        private Integer value;

        private Long rank;

        public DemoRankObject(Integer value) {
            this.value = value;
        }

        public Integer getValue() {
            return value;
        }

        public void setValue(
                Integer value) {
            this.value = value;
        }

        public Long getRank() {
            return rank;
        }

        public void setRank(
                Long rank) {
            this.rank = rank;
        }

        @Override
        public String toString() {
            return "DemoRankObject [value=" + value + ", rank=" + rank + "]";
        }

        @Override
        public Integer weight() {
            return value;
        }

    }
}
